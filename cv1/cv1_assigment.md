

## Diagram pro 1. cvičení ##
![Model](cv1_model.png)



## Popis hry ##

Ve hře Doodle Jump je cílem vést čtyřnohý tvor zvaný „Doodler“ směrem
nahoru po nekonečné sérii platforem bez pádu. Levá strana hracího pole je propojena 
s pravou stranou. Hráči mohou krátce zrychlit Doodlera prostřednictvím objektů jako
raketka nebo speciální platformou, která fungují jako pružina, existují také platformy,
které jsou zničeny po jednom dotyku s nimi. Také můžéme se setkat s monstry, když se dotýkáme ho,
tak doodle umře.

## Doporučený stav semestrální práce po 1. cvičení ##


* vytvořený repozitář pro semestrální práci na školním gitlabu
* pattern bridge použivaný pro snížení platformní závislosti na knihovně SDL.
  * Hra samozřejmě dál používá knihovnu SDL, ale díky návrhovému vzoru Bridge jsme schopni snadno vyměnit SLD za jiný způsob vykreslování
* výběr z vlastností:
  * po spuštění se na obrazovce objeví FIT ČVUT logo
  * logo se pohybuje při stisknutí šipek na klávesnici


