


## Diagram pro 4. cvičení ##
![Model](cv4_model.png)


## Doporučený stav semestrální práce po 4. cvičení ##
* Návrhový vzor Command pro ovládání Modelu pomocí commandů
  * Controller vytváří jednotlivé commandy
  * Musí existovat fronta dosud nezpracovaných commandů, a maté zpracovávat commandy čekající ve frontě
* návrhový vzor Memento pro ukládání stavu Modelu(kdýž doodle zemře tak se vratí k checkpointu)
* Návrhový vzor Proxy pro kontrolu přístupu k Modelu(používáme napřiklad k logovaní hrý, nebo můžete vymyšlet něco jiného)
* protorype pro clonovaný objectů
* přídaní monstry a rockety do hry, a také spawnery pro ně
* přídaní loggeru do hry