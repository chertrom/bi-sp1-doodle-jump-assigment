


## Diagram pro 3. cvičení ##
![Model](cv3_model.png)


## Doporučený stav semestrální práce po 3. cvičení ##

* Model vytváří herní objekty pomocí vzoru AbstractFactory
* doodle vytváří pomocí doodle builder(používaný pattern builder, mužeme vytvařet různé složitostí)
* použiváme spawner pro planky(stratagy)
* Observer pattern použitý pro notifikace mezi Modelem a View
* realizacé skokovou logiky pro doodle 
* pattern singleton pro config(které koliv parametrý můžeme dat do configu, napříkald velikost displejí, nebo rýchlost doodle)
* nakonec doodle musí umět skakat nekonečně dlouho